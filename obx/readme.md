# 1. Add domain

```shell
$ nano /mnt/c/Windows/System32/drivers/etc/hosts
```

```
        # reverse-proxy for OBX front end:
        127.0.0.1                               obxl.book4time.eu
```



# 2. Setup reverse proxies [IIS]

## 2.1 Match API calls 

```
^api/(.*)
http://localhost:9004/api/{R:1}
```

## 2.2 Match config.json

```
assets/js/config.json
http://127.0.0.1:3000
```

## 2.3 Match page requests

```
^(?!api\/)(.*)
http://127.0.0.1:4200/{R:1}
```

## 2.4 Match assets

```
(.*.(js|map|ico|css|svg|png))$
http://127.0.0.1:4200/{R:1}
```



# 3. Clone repositories

```shell
$ git clone git@bitbucket.org:jacksdiao/project-config.git
$ git clone git@bitbucket.org:b4tdev/online-booking-x.git
```



# 4. Start client server
```shell
$ cd online-booking-x/ClientApp
$ npm i
$ npm run start -- --port=4200 --baseHref=/ --disableHostCheck=true
```



# 5. Start backend servers

```shell
$ cd online-booking-x/OnlineBookingX.Web/OnlineBookingX.Web
$ node ../../../project-config/obx/index.js
```

- this starts the config server that enables other devices (on the same network) to access the app, the response is

```javascript
// apiBaseUrl always point at the same domain as the request
{
    "apiBaseUrl": "<protocol>://<hostname-or-ip>"
}
```

- this also starts the dotnet API, which is equivalent to:

```shell
$ dotnet.exe run --launchProfile OnlineBookingX.Web
# note, it's possible to use --urls 'http://localhost:1234' option
```

