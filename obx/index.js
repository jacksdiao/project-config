const express = require("express");
const cors = require("cors");
const { spawn } = require("child_process");
const { argv } = require("yargs");
const { green, red, yellow } = require("chalk");
const { dirname, join } = require("path");

const configServer = express(cors());
const port = 3000;

function getApiBaseUrl(headers) {
  // get the hostname prior to reverse-proxy
  if (headers["referer"]) {
    const [protocol, hostname] = headers["referer"].split(/:?\/+/);
    return `${protocol}://${hostname}`;
  }

  // best guess
  const [hostname] = headers["x-forwarded-for"].split(":");
  return `https://${hostname}`;
}

/**
 * @desc OBX config server
 */
function configObxConfigServer() {
  configServer.get("/obx/config", (req, res) => {
    const apiBaseUrl = getApiBaseUrl(req.headers);
    res.json({
      forceSupportActivities: true,
      b4TGoogleAnalyticsId: "UA-178025653-1",
      apiBaseUrl,
      // translationResourceBaseUrl:
      //   "https://obximages.s3.us-east-1.amazonaws.com/lang/q",
      logSettings: {
        logLevelConsole: 5,
        // logLevelConsole: 1,
        logLevelServer: 5,
        logApiUrl: `${apiBaseUrl}/api/Logging/PostNGXLogEntry`,
        enableSourceMaps: true,
        overrideSessionInMins: 5,
      },
      pendoConfig: {
        pendoApiKey: "d0ea3984-b017-4626-46d7-4c0c45dcafcd",
        startPendoWaitMs: 5000,
        applicationName: "OBX (Dev)",
      },
      cookiebotConfig: {
        cookiebotApiKey: "",
      },
      translationResourceBaseUrl: `${apiBaseUrl}/assets/lang`,
      translationResource: {
        "de-DE": "de-DE.json",
        "en-US": "en-US.json",
        "es-ES": "es-ES.json",
        "fr-FR": "fr-FR.json",
        "ja-JP": "ja-JP.json",
        "ko-KR": "ko-KR.json",
        "pl-PL": "pl-PL.json",
        "pt-BR": "pt-BR.json",
        "pt-PT": "pt-PT.json",
        "ru-RU": "ru-RU.json",
        "sv-SE": "sv-SE.json",
        "zh-CN": "zh-CN.json",
        "zh-TW": "zh-TW.json",
        "ca-ES": "ca-ES.json",
        "it-IT": "it-IT.json",
        "nl-NL": "nl-NL.json",
        "tr-TR": "tr-TR.json",
      },
      useFallbackTranslation: false,
      fallbackLocales: ["en-US"],
      activitySearchNextNumberOfDays: 14,
      activitySearchLookAheadNumberOfDays: 14,
      specialActivityAccountSearchNextNumberOfDays: 7,
      specialActivityMerchantCode: "spadev",
      showTraceableErrorCode: true,
    });
  });

  configServer.get("/throttle", (req, res) => {
    setTimeout(
      () =>
        res.json({
          message: "whoa, you waited long enough to just to see this message",
        }),
      10000,
    );
  });

  configServer.get("/business-error", (req, res) => {
    res.status(500);
    res.render("error", { error: "some business error" });
  });

  configServer.use("/static", express.static(join(__dirname, "static")));
  configServer.use(
    "/npm",
    express.static(join(dirname(__dirname), "node_modules")),
  );
}

/**
 * @desc OBX API server
 */
function startObxDotNetServer() {
  const { launchProfile, urls } = argv;
  let inputs = ["run"];

  if (launchProfile) {
    inputs.push(`--launchProfile=${launchProfile}`);
  } else {
    inputs.push("--launchProfile=OnlineBookingX.Web");
  }

  if (urls) {
    inputs.push(`--urls=${urls}`);
  }

  const cmd = spawn("dotnet.exe", inputs);

  /*
  [VRB] This is a verbose statement
  [DBG] This is a debug statement
  [INF] This is a info statement
  [WRN] This is a warning statement
  [ERR] This is an error statement
  [FTL] This is a fatal statement
  */
  cmd.stdout.on("data", function (logBuffer) {
    console.log(
      logBuffer
        .toString()
        .replace(/\[inf\]|\[vrb\]/gi, (match) => green(match))
        .replace(/\[wrn\]|\[dbg\]/gi, (match) => yellow(match))
        .replace(/\[err\]|\[ftl\]/gi, (match) => red(match)),
    );
  });

  cmd.stderr.on("data", function (logBuffer) {
    console.log(red(logBuffer.toString()));
  });

  cmd.on("exit", function (code) {
    console.log("child process exited with code " + code.toString());
  });
}

/**
 * @desc MAIN
 */
configObxConfigServer();
configServer.listen(port, () =>
  console.log(`config server listening at http://localhost:${port}`),
);

startObxDotNetServer();
