(async () => {
  // if ResizeObserver is not supported by the browser
  if ("ResizeObserver" in window === false) {
    // then load polyfill asynchronously
    const module = await import(
      "/npm/resize-observer-polyfill/dist/ResizeObserver.es.js"
    );
    window.ResizeObserver = module.default;
  }
})();
