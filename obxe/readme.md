# 1. Add domain

```shell
$ nano /mnt/c/Windows/System32/drivers/etc/hosts
```

```
        # reverse-proxy for OBX front end:
        127.0.0.1                               obxel.book4time.eu
```



# 2. Setup reverse proxies [IIS]

## 2.1 Match config.json

```
assets/js/config.json
http://127.0.0.1:3002
```

## 2.2 Match page requests

```
^(?!api\/)(.*)
http://127.0.0.1:4202/{R:1}
```

## 2.3 Match assets

```
(.*.(js|map|ico|css|svg|png|json))$
http://127.0.0.1:4202/{R:1}
```



# 3. Clone repositories

```shell
$ git clone git@bitbucket.org:jacksdiao/project-config.git
$ git clone git@bitbucket.org:b4tdev/online-booking-x.git
```



# 4. Start client server
```shell
$ cd online-booking-x/OBXEditor
$ npm i
$ npm run start -- --port=4202 --baseHref=/ --disableHostCheck=true
```

