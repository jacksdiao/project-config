const express = require("express");
const cors = require("cors");

const configServer = express(cors());
const port = 3002;

function getApiBaseUrl(headers) {
  // get the hostname prior to reverse-proxy
  if (headers["referer"]) {
    const [protocol, hostname] = headers["referer"].split(/:?\/+/);
    return `${protocol}://${hostname}`;
  }

  // best guess
  const [hostname] = headers["x-forwarded-for"].split(":");
  return `https://${hostname}`;
}

/**
 * @desc OBX Editor config server
 */
function configObxeConfigServer() {
  configServer.get("/obxe/config", (req, res) => {
    const apiBaseUrl = getApiBaseUrl(req.headers);
    res.json({
      apiBaseUrl,
      // translationResourceBaseUrl:
      //   "https://obximages.s3.us-east-1.amazonaws.com/lang/q",
      logSettings: {
        logLevelConsole: 5,
        // logLevelConsole: 1,
        logLevelServer: 5,
        logApiUrl: `${apiBaseUrl}/api/Logging/PostNGXLogEntry`,
        enableSourceMaps: true,
        overrideSessionInMins: 5,
      },
      translationResourceBaseUrl: `${apiBaseUrl}/assets/lang`,
      translationResource: {
        "de-DE": "de-DE.json",
        "en-US": "en-US.json",
        "es-ES": "es-ES.json",
        "fr-FR": "fr-FR.json",
        "ja-JP": "ja-JP.json",
        "ko-KR": "ko-KR.json",
        "pl-PL": "pl-PL.json",
        "pt-BR": "pt-BR.json",
        "pt-PT": "pt-PT.json",
        "ru-RU": "ru-RU.json",
        "sv-SE": "sv-SE.json",
        "zh-CN": "zh-CN.json",
        "zh-TW": "zh-TW.json",
      },
      useFallbackTranslation: false,
      fallbackLocales: ["en-US"],
    });
  });
}

/**
 * @desc MAIN
 */
configObxeConfigServer();
configServer.listen(port, () =>
  console.log(`config server listening at http://localhost:${port}`)
);

