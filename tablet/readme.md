# 1. Client

*local instance*:

```shell
$ ng serve --host=0.0.0.0 --port=4202 --public-host=pht528.book4time.eu --baseHref=/tablet
```

*config.json*:

```json
{
    "api_base": "book4time.eu/book4timeapi2qa/api/"
}
```



*config.json* for serving through *avahi*:

```json
{
    "api_base": "jdiao.local/book4timeapi2qa/api/"
}
```



# 2. Setup reverse proxies [IIS]

## 2.1 Match API calls 

```
book4timeapi2qa\/(api.*)
http://127.0.0.1/book4timeapi2/{R:1}
```

## 2.2 Match page requests

```
(tablet[#]?\/.*)
http://127.0.0.1:4200/{R:1}
```

## 2.3 Match assets

```
(assets\/.*)
http://127.0.0.1:4200/{R:1}
```

## 2.4 Webpack

```
(sockjs-node\/.*)
http://127.0.0.1:4200/{R:1}
```

