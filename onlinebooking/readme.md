*local instance*:

```shell
$ ng serve --host=0.0.0.0 --port=4201 --public-host=pht528.b4tlocal.com --baseHref=/onlinebooking
```

*config.json*:

```json
{
    "api_base": "b4tlocal.com/book4timeapi2/api/"
}
```

*config.json* for serving through *avahi*:

```json
{
    "api_base": "jdiao.local/book4timeapi2/api/"
}
```
